---
layout: default
saison: 53
date: '2024-04-30'
ligaspieltag: 4
step: 1
finals_step: 2
finalteams: 4
managervoting: 
  enabled: false
  result: ''
startliste:
  -
    - 626
    - 1093
    - 907
    - 1074
  - 
    - 2242
    - 2314
    - 2339
    - 2753
startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 3
  - 1 
  - 2
  - 0
---
