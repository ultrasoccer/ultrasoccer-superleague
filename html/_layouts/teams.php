<?php

ini_set('display_errors', 1);
ini_set('log_errors',1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

define( 'SAISON', '{{ page.saison }}' );
define( 'SAISON_NOW', '{{ site.data.global.saison_now }}' );

include_once('functions/db_connect.php');
include_once('functions/db_methods.php');

?>
<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="cache-control" content="no-cache" />
  <meta http-equiv="pragma" content="no-cache" />
  <meta http-equiv="expires" content="0" />
  <link rel="stylesheet" href="assets/css/all.min.css">
  <link rel="stylesheet" href="assets/css/custom.css">
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/utils.js"></script>
  <script>
    var TEAMS=json_load('functions/get_teams.php');
    var STARTLISTE_PARTIEN_DB={{ site.data.partien | jsonify  }};
  </script>
</head>

<body>
  {{ content }}
</body>