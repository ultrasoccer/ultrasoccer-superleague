<?php

ini_set('display_errors', 1);
ini_set('log_errors',1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

//
//define( 'SAISON_NOW', '{{ site.data.global.saison_now }}' );

include_once('functions/db_connect.php');
include_once('functions/db_methods.php');
db_removeUserExpiredIp();

if( !empty($_POST['csv']) ) $csv=$_POST['csv'];
if( !empty($_POST['p']) ) $PARTIEN = json_decode( $_POST['p'], true );
if( !empty($_POST['t']) ) define( 'TURNIER', $_POST['t'] );
if( !empty($_GET['t']) ){
  define( 'TURNIER_ID', $_GET['t'] );
} else {
  define( 'TURNIER_ID', db_getLastTurnier() );
}

$secret='';

// check user logged in 
$loguser = db_getUserByIp();

/*
if( !empty($loguser) ){
  ?><p><a href="teams.php">Teams</a> | <a href="turnier.php">Turnier</a></p><?
}
*/

function findMatch(){
  foreach($PARTIEN as $partie){
    var_dump($partie);
  }
}


if( !empty($csv) && !empty($loguser) ){

  //$secret=$_POST['secret'];
  //$user=db_checkSecret($secret);

  $user = $loguser['user'];
  if( $user ){
    $C=preg_split('/
/',$csv);
    if( trim($C[0]) == 'runde,spieltag,T,H,A,TH,TA,MID,Teaminfo' ){
      foreach( $C as $i=>$v ){
        if( $i > 0 ) db_addErgebnisse($v,TURNIER);
      }
    } else if( trim($C[0]) == 'Saison,Spieltag,MatchID,teamID,L,Pos,SpielerID,Spieler,Z+,Z-,T,A,Bk' ) {
      $H=0; $A=0; $T=''; $turnierID=0; $runde=0; $spieltag=0; $TH=0; $TA=0; $MID=0;
      $St = [];
      foreach( $C as $i=>$v ){
        if( $i > 0 ) $St[]=$v; 
        $V = preg_split('/,/',$v);
        if( $V[4] == 'H' && $V[5]*1 > 0 ){
          $TH+=$V[10]*1;
          $H  =$V[3]*1;
        } 
        if( $V[4] == 'G' && $V[5]*1 > 0 ){
          $TA+=$V[10]*1;
          $A  =$V[3]*1;
        } 
        $MID=$V[2];
      }
      $inv = false;
      $r = array_filter($PARTIEN, 
          function($v,$k){
            global $H; 
            global $A;
            global $inv;
            if( $v['MID']*1 == 0 && $v['H']*1 == $H && $v['A']*1 == $A ){
              return true;
            } else if( $v['MID']*1 == 0 && $v['H']*1 == $A && $v['A']*1 == $H ){
              $inv = true;
              return true;
            }
          },ARRAY_FILTER_USE_BOTH
        );
      if( sizeof($r) > 0 ){
        $r = array_values($r)[0];
        $runde = $r['runde'];
        $spieltag = $r['spieltag'];
        $T=$r['T'];
        $turnierID=$r['turnierID'];
        if( $inv == true ){
          db_addErgebnisse(implode( ',', [ $runde, $spieltag, $T, $A, $H, $TA, $TH, $MID, '' ] ), $turnierID);
        } else {
          db_addErgebnisse(implode( ',', [ $runde, $spieltag, $T, $H, $A, $TH, $TA, $MID, '' ] ), $turnierID);
        }
        
        foreach( $St as $st ){
          db_addStatistik($st,$secret);
        }
      }
    } else if( trim($C[0]) == 'TeamID,N,SpielerID,Attr,Spieler,Alter,Pos,S,T,A,Z+,Z-,Zd' ){
      $TEAMS=db_getIdNtFromTeams();
      foreach( $C as $i=>$v ){
        $w=preg_split('/,/',$v);
        if( !empty($TEAMS[$w[0]]) ){
          if( $TEAMS[$w[0]] == $w[1] ){
            //db_addSpieler($v,$secret);
          }
        }
      }
    } else if( trim($C[0]) == 'SpielerID,Spieler,Alter,Nt,Saison,Liga,TeamID,Team,S,T,A,Z+,Z-,Zd,Bk,Bi+,Bi-,Attr,Face,Karriereende' ){
      db_addSpieler(array_pop($C),$secret);
    }
  }

}

?>
<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="cache-control" content="no-cache" />
  <meta http-equiv="pragma" content="no-cache" />
  <meta http-equiv="expires" content="0" />
  <link rel="stylesheet" href="assets/css/all.min.css">
  <link rel="stylesheet" href="assets/css/custom.css">
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/utils.js"></script>
  <script src="assets/js/script3.js"></script>
</head>

<body>
{{ content }}
  <script>
    var TURNIER_ID = getQueryVariable('t');
    if( TURNIER_ID == undefined ) TURNIER_ID = <?= TURNIER_ID ?>;
    TURNIER_ID = TURNIER_ID ? TURNIER_ID : TURNIER[0].id;
    var Turnier = TURNIER.filter((a) => (a.id == TURNIER_ID))[0];
    Turnier.partien_abstand = JSON.parse(Turnier.partien_abstand);
    Turnier.finale_abstand = JSON.parse(Turnier.finale_abstand);
    var Turnier_steps = Turnier.partien_abstand.concat(Turnier.finale_abstand);
    //var SAISON_SETTINGS={{ site.pages | where_exp: 'item','item.saison>25' | sort: 'saison' | map: 'finalteams' | jsonify }};
    //var DATA={{ site.data | jsonify }};
    var SAISON=Turnier.start_saison;
    //var SAISON_NOW='{{ site.data.global.saison_now }}';
    var MANAGERVOTING={{ page.managervoting.enabled | default: false }};
    var MANAGERVOTING_TEAM='{{ page.managervoting.result }}'; {% assign M = site.pages |  where_exp: "item", "item.saison > 25" %}
    var MANAGERVOTING_WINNER={ {% for m in M %}"{{ m.saison }}" : {{ m.managervoting.result | jsonify }}{% if forloop.last == false %},{% endif %}{% endfor %} };
    var SAISON_TEAMVOTED=json_load('functions/get_managervoted.php?saison='+SAISON);
    SAISON_TEAMVOTED.push([]);

    var TEAMS=json_load('functions/get_teams.php');
    //var T2ID={};
    var FINALTEAMS=Turnier.finalteams;
    //TEAMS.forEach(
    //  function(v){
    //    T2ID[v.nt] = v.id;
    //  }
    //);
    //var STARTLISTE={{ page.startliste | jsonify }}; {% assign startliste_size = page.startliste[0] | size | append: '' %}
    var STARTLISTE=[];
    var TURNIER_TEAMS = <?= json_encode( db_getFromTurnier_teams(TURNIER_ID) ) ?>;
    var S = TURNIER_TEAMS.forEach(
      function(v){
        if( STARTLISTE[v.gruppe*1-1] == undefined ) STARTLISTE[v.gruppe*1-1]=[]
        STARTLISTE[v.gruppe*1-1].push(v.teamID*1);
      }
    );
    //var STARTLISTE={{ page.startliste | jsonify }}; {% assign startliste_size = page.startliste[0] | size | append: '' %}
    
    var STARTLISTE_PARTIEN_DB={{ site.data.partien | jsonify  }};
    var STARTLISTE_PARTIEN_PAUSE={{ page.startliste_partien_pause | jsonify }};
    var STARTLISTE_PARTIEN = STARTLISTE_PARTIEN_DB[ STARTLISTE[0].length ];

    if( $.isArray( STARTLISTE_PARTIEN_PAUSE ) ){
      STARTLISTE_PARTIEN.forEach(
        function(v,i){
          if( STARTLISTE_PARTIEN_PAUSE[i] != undefined ){
            STARTLISTE_PARTIEN[i][0]=STARTLISTE_PARTIEN_PAUSE[i];
          }
        }
      );
    }
    var SPIELER=json_load('functions/get_spieler.php');
    var UNKNOWN=json_load('functions/get_unknown.php');
    if( UNKNOWN.length == 0 ) UNKNOWN=[[''],['']];

    UNKNOWN = UNKNOWN.reduce((a,b) => (a.concat(b)));

    var NEUESPIELER=json_load('functions/get_neuespieler.php');
    var ERGEBNISSE=json_load('functions/get_ergebnisse.php');
    //var STAT_SAISON=json_load('functions/get_statistik_by_saison.php?saison='+SAISON);
    var STAT_SAISON=json_load('functions/get_statistik_by_turnier.php?turnier='+TURNIER_ID);
    var STAT_ALL=json_load('functions/get_statistik.php');
    STATISTIK_SAISON={};

    //var MEISTERTITEL=calculateAllPunktestand().map((a) => ( a[0].nt ));
    var MEISTERTITEL=[];

    
    STAT_SAISON.forEach(
      function(v,i){
        if( STATISTIK_SAISON[v.SpielerID] == undefined ) STATISTIK_SAISON[v.SpielerID]={ 'S' :0, 'T' : 0, 'A' : 0, 'Z' : 0, 'Zpos' : 0, 'Zneg' : 0, 'MID' : [], 'face' : '' };
        STATISTIK_SAISON[v.SpielerID].S++;
        STATISTIK_SAISON[v.SpielerID].T+=v.Tore*1;
        STATISTIK_SAISON[v.SpielerID].A+=v.Assists*1;
        STATISTIK_SAISON[v.SpielerID].Z+=v.Zwk*1;
        STATISTIK_SAISON[v.SpielerID].Zpos+=v.Zpos*1;
        STATISTIK_SAISON[v.SpielerID].Zneg+=v.Zneg*1;
        STATISTIK_SAISON[v.SpielerID].MID.push(v.MID);
        STATISTIK_SAISON[v.SpielerID].name=v.name;
        STATISTIK_SAISON[v.SpielerID].age=v.age*1;
        STATISTIK_SAISON[v.SpielerID].age_saison=v.age_saison*1;
        STATISTIK_SAISON[v.SpielerID].nt=v.nt;
        STATISTIK_SAISON[v.SpielerID].teamId=v.id;
        STATISTIK_SAISON[v.SpielerID].attr=v.attr;
        STATISTIK_SAISON[v.SpielerID].face=v.face;
      }
    );

    STATISTIK_ALL={};
    STAT_ALL.forEach(
      function(v,i){
        if( STATISTIK_ALL[v.SpielerID] == undefined ) STATISTIK_ALL[v.SpielerID]={ 'S' :0, 'T' : 0, 'A' : 0, 'Z' : 0, 'Zpos' : 0, 'Zneg' : 0, 'MID' : [], 'face' : '' };
        STATISTIK_ALL[v.SpielerID].S++;
        STATISTIK_ALL[v.SpielerID].T+=v.Tore*1;
        STATISTIK_ALL[v.SpielerID].A+=v.Assists*1;
        STATISTIK_ALL[v.SpielerID].Z+=v.Zwk*1;
        STATISTIK_ALL[v.SpielerID].Zpos+=v.Zpos*1;
        STATISTIK_ALL[v.SpielerID].Zneg+=v.Zneg*1;
        STATISTIK_ALL[v.SpielerID].MID.push(v.MID);
        STATISTIK_ALL[v.SpielerID].name=v.name;
        STATISTIK_ALL[v.SpielerID].age=v.age*1;
        STATISTIK_ALL[v.SpielerID].age_saison=v.age_saison*1;
        STATISTIK_ALL[v.SpielerID].nt=v.nt;
        STATISTIK_ALL[v.SpielerID].teamId=v.id;
        STATISTIK_ALL[v.SpielerID].attr=v.attr;
        STATISTIK_ALL[v.SpielerID].face=v.face;
      }
    );

    const STEP={{ page.step | default: '1' }};
    const FINALS_STEP={{ page.finals_step | default: '0' }};
    const LIGASPIELTAG={{ page.ligaspieltag | default: '1' }};
    const SAISON_START_DATE=new Date( '{{ page.date }}' ).toJSON();

    // check ob alle Teams der Startliste in DB vorhanden sind
    var TIDs = TEAMS.map((a) => (a.id*1));
    var check=
    STARTLISTE.map(
      (a) => (
        a.map( 
          (b) => ( TIDs.indexOf(b) > -1 )
        )
      )
    ).reduce(( c,d ) => ( c && d )).reduce(( e,f ) => ( e && f ));

    if( check ){
      $(document).ready( function(){
          //calculatePartien('{{ page.saison }}', '{{ page.date }}');
          calculatePartien(Turnier, Turnier.start_datum);
          calculatePunktestand(Turnier, '7');  // ######## check this
          calculateTabelle(Turnier.start_saison,'T');
          calculateTabelle(Turnier.start_saison,'A');
          calculateTabelle(Turnier.start_saison,'Z');

          $('h4.tabelle button').on('click',
            function(){
              $('h4.tabelle button').toggleClass('active',false);
              $(this).toggleClass('active',true);
              $('table.tabelle').toggleClass('d-none',true);
              $('#tabelle_'+$(this).text()[0]).toggleClass('d-none',false);
            }
          );
          $('#news tbody').html('');
          UNKNOWN.forEach(

            function(v,i){

              var team=TEAMS.filter((a) => ( a.nt == v.nt ))[0];
              if( team == undefined ) return;

              if( v.nt == 'xx' ) return;

              var tr = document.createElement('tr');

              var td = document.createElement('td');

              var st=parseInt(( new Date() - new Date(v.zeitstempel) ) / 1000 / 3600 / 24);

              $(td).text( st == 1 ? 'vor ' + st + ' Tag' : 'vor ' + st + ' Tagen' );
              $(tr).append(td);

              var td = document.createElement('td');
              td.innerText='';
              $(tr).append(td);

              var td = document.createElement('td');
              var img = document.createElement('img');
              img.src='images/flags/' + ( v.flag ? v.flag : v.nt ) + '.svg';
              img.classList='flags';
              $(td).append(img);
              $(tr).append(td);

              var td = document.createElement('td');
              td.classList='mono';
              var a = document.createElement('a');
              a.href='https://ultrasoccer.de/playerprofile/?id='+v.SpielerID;
              a.target='ultrasoccer';
              $(a).text(v.SpielerID);
              $(td).append(a);
              $(tr).append(td);

              var td = document.createElement('td');
              var a = document.createElement('a');
              a.href='https://ultrasoccer.de/players/?id='+v.teamID;
              a.target='ultrasoccer';
              $(a).text(v.team);
              $(td).append(a);
              $(tr).append(td);

              $('#news tbody').append(tr);

            }
          );

          NEUESPIELER.forEach(

            function(v,i){
              
              var team=TEAMS.filter((a) => ( a.id*1 == v.teamID*1 ))[0];
              if( team == undefined ) return;

              if( STARTLISTE.reduce((a,b) => (a.concat(b))).indexOf( v.teamID*1 ) == -1 ) return;

              var tr = document.createElement('tr');

              var td = document.createElement('td');

              var st=parseInt(( new Date() - new Date(v.zeitstempl) ) / 1000 / 3600 / 24);


              $(td).text( st == 1 ? 'vor ' + st + ' Tag' : 'vor ' + st + ' Tagen' );
              $(tr).append(td);

              var td = document.createElement('td');
              var div = document.createElement('div');
              div.classList='face';

              if( v.face.split(/_/g).length == 5 ){
                var img = document.createElement('img');
                img.src='images/facegenerator/fg_head_' + v.face.split(/_/g)[0] + '.png';
                $(div).append(img);

                var img = document.createElement('img');
                img.src='images/facegenerator/fg_hair_' + v.face.split(/_/g)[1] + '.png';
                $(div).append(img);

                var img = document.createElement('img');
                img.src='images/facegenerator/fg_beard_' + v.face.split(/_/g)[2] + '.png';
                $(div).append(img);

                var img = document.createElement('img');
                img.src='images/facegenerator/fg_mouth_' + v.face.split(/_/g)[3] + '.png';
                $(div).append(img);

                var img = document.createElement('img');
                img.src='images/facegenerator/fg_eyes_' + v.face.split(/_/g)[4] + '.png';
                $(div).append(img);
              }

              $(td).append(div);
              $(tr).append(td);

              var td = document.createElement('td');
              var img = document.createElement('img');
              
              //img.src='images/flags/' + ( team.flag ? team.flag : team.nt ) + '.svg';
              img.src='images/flags/' + v.nt + '.svg';

              img.classList='flags';
              $(td).append(img);
              $(tr).append(td);

              var td = document.createElement('td');
              var a = document.createElement('a');
              a.href='https://ultrasoccer.de/playerprofile/?id='+v.SpielerID;
              a.target='ultrasoccer';
              if( v.attr != '' ){
                Object.values( v.attr.split(/ /g) ).forEach(
                  function(w){
                    var attr = document.createElement('i');
                    attr.classList='fas '+w;
                    $(td).append(attr);
                  }
                )
              }
              $(a).text(v.name);
              var span=document.createElement('span');
              $(span).text(' ('+v.age+')');

              $(td).append(a);
              $(td).append(span);
              $(tr).append(td);

              var td = document.createElement('td');
              var a = document.createElement('a');
              a.href='https://ultrasoccer.de/players/?id='+v.teamID;
              a.target='ultrasoccer';
              $(a).text(team.name);
              $(td).append(a);
              $(tr).append(td);


              $('#news tbody').append(tr);

            }
          );



        }
      );
    } else {
      alert( 'Error: Nicht alle Teams der Startliste sind in der DB vorhanden!');
    }

  </script>


  <? if( !empty($loguser) ){ ?>
  <form id="formu" method="POST">
  <div id="copypaste" class="d-none">
    <button type="button" onclick="$('#t').val(Turnier.id); $('#p').val(JSON.stringify( PARTIEN )); $('#formu').submit();">update</button>
    <button type="button" onclick="$('#copypaste').toggleClass('d-none');">exit</button>
    <input  type="text" class="d-none" name="secret" placeholder="secret" value="<?=$secret?>">
    <input type="hidden" id="t" name="t" value="">
    <input type="hidden" id="p" name="p" value="">
    <textarea name="csv"></textarea>
  </div>
  </form>
  <? } ?>

  <div class="div_right">
    <div id="saisons">
      <button id="historie" class="d-none" onclick="calculateHistorie()"><b>HISTORIE</b></button> <button id="ewige" class="d-none" onclick="calculateEwige()"><b>EWIGE</b></button>
    </div>
    <h4 class="tabelle">Tabelle <button>Zweikampf</button><button>Assists</button><button>Tore</button><button class="punktestand active">Punktestand</button></h2>
    <table id="tabelle_P" class="tabelle">
      <thead>
        <tr>
          <th>Pl.</th>
          <th>Sp.</th>
          <th>Team</th>
          <th><i class="fas fa-trophy" title="Pokalgewinn"></i></th>
          <th style="display: none;"><i class="fas fa-crown" title="Meistertitel"></i></th>
          <th style="display: none;"><i class="fas fa-star" title="im Finale gescheitert"></i></th>
          <th style="display: none;"><i class="fas fa-thumbs-up managervoted" title="von Managern zum Team des Jahres gewählt"></i></th>
          <th title="Siege">S</th>
          <th title="Unentschieden">U</th>
          <th title="Niederlagen">N</th>
          <th>Hist.</th>
          <th>Tore</th>
          <th>Dif</th>
          <th>Zwk</th>
          <th>Pkt.</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>

    <table id="tabelle_T" class="tabelle d-none">
      <thead>
        <tr>
          <th>Pl.</th>
          <th>Sp.</th>
          <th></th>
          <th>Name</th>
          <th>Team</th>
          <th>T</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>

    <table id="tabelle_A" class="tabelle d-none">
      <thead>
        <tr>
          <th>Pl.</th>
          <th>Sp.</th>
          <th></th>
          <th>Name</th>
          <th>Team</th>
          <th>A</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>

    <table id="tabelle_Z" class="tabelle d-none">
      <thead>
        <tr>
          <th>Pl.</th>
          <th>Sp.</th>
          <th></th>
          <th>Name</th>
          <th>Team</th>
          <th>Z</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>

    <div class="scrollen_team">
      <table id="tabelle_team" class="tabelle d-none" ondblclick="backToActiveTable()">
        <thead>
          <tr>
            <th></th>
            <th>Nt</th>
            <th>Spieler</th>
            <th>Alter</th>
            <th>S/T/A</th>
            <th>Zwk</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
    <div class="scrollen">
      <table id="tabelle_historie" class="tabelle d-none" ondblclick="backToActiveTable()">
        <thead>
          <tr>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>

    <h3 style="display: '';" class="news">Spielerdebüts</h3>
    <table style="display: '';" id="news" class="news">
      <thead>
        <th>Zeit</th>
        <th></th>
        <th>Nt</th>
        <th>Spieler</th>
        <th>Team</th>
      </thead>
      <tbody>

      </tbody>
    </table>

  </div>

  <div id="partien" class="div_left">
  <?
    if( !empty($loguser) ){
      ?><p><a href="teams.php">Teams</a> | <a href="turnier.php">Turnier</a></p><?
    }
  ?>
  </div>

  <div id="lastresults" class="d-none" ondblclick="hideLastResults(true)">
    <table>
      <tbody>
      </tbody>
    </table>
  </div>

</body>