<?php
header('Content-Type: application/json');
ini_set('display_errors', 1);
ini_set('log_errors',1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

include_once('db_connect.php');
include_once('db_methods.php');
?>
{{ content }}
