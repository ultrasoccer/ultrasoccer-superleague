---
---
const DAY=24*3600*1000;

var _e = (el) => ( document.createElement(el) );
var _a = function(n){ var a=[]; for( i=1; i<=n; i++) a.push(i); return a; };
var _b = function(A){ var a=[]; while( A.length > 0){ a.push( A.pop() ); a.push( A.shift() ); } return a };
var _done = (a) => ( a.reduce( (a,b) => ( a && b ) ) );


function calculatePartien_round(turnier,date,steps,runde,runde_label,Grps,Grps_1){

  $('#partien').append('<h2 class="saison s_'+turnier.start_saison+' id_'+turnier.id+'">Saison '+turnier.start_saison+'</h2>');

  // #######################################################################
  // Erstelle Partien

  var done=[];

  // stelle Gruppen zusammen
  STARTLISTE.forEach(
    function(g){
      var G=[];
      g.forEach(
        function(v){
            G.push( TEAMS.filter( (a) => (a.id == v) )[0] );
        }
      );
      Grps.push(G);
    }
  );

  var h3= _e('h3');
  h3.classList = 'runde id_'+turnier.id+' s_'+turnier.start_saison+' r_'+runde;
  h3.innerHTML = 'Runde '+runde+ ' ('+runde_label+')';
  $('#partien').append(h3);
  
  // erzeuge die Partien und prüfe, ob diese bereits gespielt wurden
  STARTLISTE_PARTIEN.forEach(
    function(v,i){
      done.push( grpPartien(Grps,date,steps,turnier,runde,i+1,v.slice(1)) ); steps+=Turnier_steps.shift()*1;
    }
  );

  return [ done[0], steps, done[1] ];

}

var _runde_label='';

function calculatePartien_finals(final,turnier,date,steps,runde,runde_label,done,Grps,Grps_1){

  if( final == FINALTEAMS ){
    if( _done(done) ){
      //var Tabelle = calculatePunktestand( turnier,runde,false );
      //Grps_1.push( Tabelle[0].shift() );
    } else {
      Grps = [];
      STARTLISTE.forEach(
        function(w,g){
          for( var i = 0; i < FINALTEAMS / STARTLISTE.length; i++ ){
            Grps.push(
              {
                'kontinent' : 'dummy',
                'id': 0,
                'nt': 'xx',
                'name': '<i>Gruppe ' + (g+1) + '.'+ (i+2) + '</i>',
                'manager': 'dummy',
                'start': 53
              }
            );
          }
        }
      );
    } 
  } else if ( final < FINALTEAMS ){
    if( _done(done) ){
    } else {
      Grps = [];
      _a(final).forEach(
        function(i){
          Grps.push(
            {
              'kontinent' : 'dummy',
              'id': 0,
              'nt': 'xx',
              'name': '<i>' + _runde_label + ' ' + (i) + '</i>',
              'manager': 'dummy',
              'start': 53
            }
          );
        }
      );
    }
  }

  var h3 = _e('h3');
  h3.classList = 'runde id_'+turnier.id+' s_'+turnier.start_saison+' r_'+runde;
  h3.innerText = runde_label;
  $('#partien').append(h3);

  //var Grps = [];
  //if( Tabelle == undefined ) Tabelle = done;
  //_a(final).forEach(
  //  function(v){
  //    Grps.push( Tabelle[0].shift() );
  //  }
  //);

  if( final == 1 ){
    Grps.push( 
      Grps_1.length > 0 ? Grps_1.shift() 
      : {
      'kontinent' : 'dummy',
      'id': 0,
      'nt': 'xx',
      'name': '<i>Gruppe 1.1</i>',
      'manager': 'dummy',
      'start': 53
      } ); 
    final=2;   
  }
  if( Grps.length > 1 ) Grps = [ Grps ];
  var done = [];
  var result = grpPartien( Grps,date,steps,turnier,runde, 1, _b(_a(final)) );
  done.push( result ); steps+=Turnier_steps.shift()*1;
  _runde_label = runde_label; 

  return [ done[0], steps, done[1] ];

}


function calculatePartien(turnier,datestring){
  
  var date=new Date(datestring).getTime();
  var done=false;
  var steps=0;
  var Grps = [];
  var Grps_1 = [];

  [ done, steps, Grps ] = calculatePartien_round(turnier,date,steps,"1","Hauptrunde",Grps, Grps_1);

  if( _done(done) ){
    var Grps = calculatePunktestand( turnier,1,false )[0];
    Grps_1.push( Grps.shift() );
  }
  
  if( FINALTEAMS >= 8 ){ 
    [ [ done, Grps ], steps ] = calculatePartien_finals(8,turnier,date,steps,"VF","Viertelfinale",done,Grps, Grps_1);
  }
  if( FINALTEAMS >= 4 ){ 
    [ [ done, Grps ], steps ] = calculatePartien_finals(4,turnier,date,steps,"HF","Halbfinale",done,Grps, Grps_1);
  }
  if( FINALTEAMS >= 2 ){ 
    [ [ done, Grps ], steps ] = calculatePartien_finals(2,turnier,date,steps,"f","Vorfinale",done,Grps, Grps_1);
  }
  [ [ done, Grps ], steps, Grps ] = calculatePartien_finals(1,turnier,date,steps,"F","Finale",done,Grps, Grps_1);

}

var St=0;
var PARTIEN = [];

function grpPartien( Grps,date,steps,turnier,runde,spieltag,Bg ){

  date = date + 3*3600000; // toJSON / UTC / Datum Kompensation  
  if( Grps.map( (a) => ( a.filter((b) => (b == undefined )).length > 0 ) )[0] ){
    alert( 'Error: Einige Teams existieren nicht in der DB' );
    return;
  }
  
  var done = [];

  var h4 = _e('h4');
  h4.classList = 'spieltag id_'+turnier.id+' s_'+turnier.start_saison+' r_'+runde+' t_'+spieltag;
  h4.id = 'r'+runde+'st'+spieltag;
  //$(h4).attr( 'ondblclick', 'getCsv('+saison+',\''+runde+'\','+spieltag+')' );
  h4.innerHTML = '<a class="unformated" href="javascript:getCsv('+turnier.start_saison+',\''+runde+'\','+spieltag+')">Spieltag ' + spieltag + '</a>';
  $('#partien').append(h4);
  //h4.innerText = 'Spieltag '+spieltag;
  //$('#partien').append(h4);
  var tbl = _e('table');
  tbl.classList = 'partien id_'+turnier.id+' s_'+turnier.start_saison+' r_'+runde+' t_'+spieltag;
  var partie=0;
  var winner=[];

  Grps.forEach(
    function(v,i){
      var BG = JSON.parse(JSON.stringify(Bg));
      do {

        var h1 = BG.shift()-1;
        var a1 = BG.shift()-1;
        partie++;

        if( h1 == -1 || a1 == -1 ) return;
        var ergebnis=ERGEBNISSE.filter( (a) => ( a.turnierID == turnier.id && a.runde == runde && a.spieltag == spieltag && a.H*1 == v[h1].id*1 && a.A*1 == v[a1].id*1 ) );

        PARTIEN.push({ 'T' : new Date(date).toJSON().slice(0,10), 'turnierID' : turnier.id, 'runde' : runde, 'spieltag' : spieltag, 'H' : v[h1].id, 'A' : v[a1].id, 'MID' : ergebnis.length == 1 ? ergebnis[0].MID : 0 });

        if( ergebnis.length == 1 ){
          
          ergebnis=ergebnis[0];
          var th=(( ergebnis.MID > 0 ) ? '<a href="https://ultrasoccer.de/match/?id='+ergebnis.MID+'" target="ultrasoccer">'+ergebnis.TH+'</a>' : ergebnis.TH );
          var ta=(( ergebnis.MID > 0 ) ? '<a href="https://ultrasoccer.de/match/?id='+ergebnis.MID+'" target="ultrasoccer">'+ergebnis.TA+'</a>' : ergebnis.TA );
          var dt=new Date(ergebnis.T).toJSON().slice(0,10);
          var st=STAT_SAISON.filter( (a) => ( a.MID == ergebnis.MID )).length == 12;
          ergebnis['partie']=partie;
          if( St != -1 ) St=spieltag;
          if( !$.isNumeric(runde) ){
            winner.push( th > ta ? TEAMS.filter((a) => (a.id*1 == ergebnis.H ))[0] : TEAMS.filter((a) => (a.id*1 == ergebnis.A ))[0] );
            done.push(true);
          } else {
            done.push(true);
          }

        } else {
          if( St > 0 ){
            var anker=St;
            setTimeout( function(){ window.location.href='#r'+runde+'st'+anker; },500 );
            St=-1;
          }
          var th='-'; var ta='-'; var dt='<i>'+new Date(date+DAY*steps).toJSON().slice(0,10)+'</i> <sup>'+(steps+Turnier.start_spieltag*1)+'</sup>'; var st=false; var match='';
          if( !$.isNumeric(runde) ){
            done.push(false);
          } else {
            done.push(false);
          }
        }

       

        var tr = _e('tr');
        tr.classList = 'begegnungen ' + (( STARTLISTE.length > 1 ) ? 'gruppe' + i : '' ) + ' h_'+v[h1].id+' a_'+v[a1].id+' '+(( st ) ? 'stats' : '');
        var td = _e('td');
        td.innerHTML = dt;
        tr.append(td);
        var td = _e('td');
        $(td).attr('data-id',v[h1].id);
        var sup = _e('sup');
        sup.innerHTML = v[h1].nt.toUpperCase();
        td.append(sup);

        var span = _e('span')
        span.innerHTML = ' '+v[h1].name.replace(/\(NL\)/g,'')+' ';
        td.append(span);
        /*
        var img = _e('img');
        img.classList = 'flags';
        img.src = './images/flags/'+(( v[h1].flag ) ? v[h1].flag : v[h1].nt.toLowerCase())+'.svg';
        td.append(img);
        */
        tr.append(td);
        
        
        var td = _e('td');
        td.innerText = ' - ';
        tr.append(td);

        var td = _e('td');
        $(td).attr('data-id',v[a1].id);
        /*var img = _e('img');
        img.classList = 'flags';
        img.src = './images/flags/'+(( v[a1].flag ) ? v[a1].flag : v[a1].nt.toLowerCase())+'.svg';
        td.append(img);
        */

        var span = _e('span')
        span.innerHTML = ' '+v[a1].name.replace(/\(NL\)/g,'')+' ';
        td.append(span);

        var sup = _e('sup');
        sup.innerHTML = v[a1].nt.toUpperCase();
        td.append(sup);
        tr.append(td);

        var td = _e('td');
        $(td).attr('data-mid',(( ergebnis.MID != undefined ) ? ergebnis.MID : '0' ) ); 
        td.innerHTML = th;
        tr.append(td);
        
        var td = _e('td');
        $(td).attr('onmouseenter', "showLastResults('"+v[h1].id+"','"+v[a1].id+"')" );
        $(td).attr('onmouseleave', 'hideLastResults()');
        $(td).attr('onclick','ignoreHideLastResults()');
        td.innerText = ':';
        tr.append(td);
        
        var td = _e('td');
        td.innerHTML = ta;
        tr.append(td);

        tbl.append(tr);
      } while( BG.length > 0 );
    },spieltag
  );

  $('#partien').append(tbl);

  return [ done, winner ];
}

function getCsv(saison, runde, spieltag){
  var tbl=$('.partien.s_'+saison+'.r_'+runde+'.t_'+spieltag)[0];
  var csv=
  Object.values(tbl.rows).map(
    function(v,i){
      var T=[];
      T.push( runde );
      T.push( spieltag );
      T.push( v.cells[0].innerText.slice(0,10) );
      T.push( v.cells[1].dataset.id );
      T.push( v.cells[3].dataset.id );
      T.push( v.cells[4].innerText );
      T.push( v.cells[6].innerText );
      T.push( v.cells[4].dataset.mid );
      T.push( TEAMS.filter((a) => (a.id == v.cells[1].dataset.id ) )[0].name + ' vs. ' + TEAMS.filter((a) => (a.id == v.cells[3].dataset.id ) )[0].name );
      return T;
    }
  ).join('\n');
  $('#copypaste').toggleClass('d-none');
  $('#copypaste textarea').text('runde,spieltag,T,H,A,TH,TA,MID,Teaminfo\n'+csv);
}

function calculatePunktestand(turnier,runde,show=true){
  
  $.each( TEAMS,
    function( i,v ){
      TEAMS[i][turnier.id] = { P: 0, Tp: 0, Tn: 0, Td: 0, Tr: ['X'], Zwk: 0, active: (( TEAMS[i].start <= turnier.start_saison )) }
    }
  );
  
  ERGEBNISSE.filter( (a) => ( ( a.turnierID == turnier.id || turnier == 0 ) && $.isNumeric(a.runde) && a.runde <= runde  ) ).forEach(
    function( v,i ){
      var H=TEAMS.filter( (a) => ( a.id*1 == v.H*1 && ( turnier.id == 0 || a.start*1 <= turnier.start_saison*1 ) ) ).sort((c,d) => (c.start*1 < d.start*1 ));
      var A=TEAMS.filter( (a) => ( a.id*1 == v.A*1 && ( turnier.id == 0 || a.start*1 <= turnier.start_saison*1 ) ) ).sort((c,d) => (c.start*1 < d.start*1 ));
      if( H.length == 0 ){
        alert('Error: Team mit Code '+v.H+' fehlt!!' );
        return;
      }
      if( A.length == 0 ){
        alert('Error: Team mit Code '+v.A+' fehlt!!' );
        return;
      }
      H=H[0][turnier.id];
      A=A[0][turnier.id];
      if( v.TH == '-' || v.TA == '-' ) return;
      if( parseInt(v.TH) > parseInt(v.TA) ){
        H.P+=3;
        H.Tr.push('S');
        A.Tr.push('N');
      } else if( parseInt(v.TH) < parseInt(v.TA) ){
        A.P+=3;
        H.Tr.push('N');
        A.Tr.push('S');
      } else {
        H.P+=1;
        A.P+=1;
        H.Tr.push('U');
        A.Tr.push('U');
      }
      H.Tp+=parseInt(v.TH); H.Tn+=parseInt(v.TA); H.Td=H.Tp-H.Tn;
      A.Tp+=parseInt(v.TA); A.Tn+=parseInt(v.TH); A.Td=A.Tp-A.Tn;
      STAT_ALL.filter((b) => ( b.MID == v.MID && v.H == b.id*1 )).forEach(
        function(c){
          H.Zwk+=c.Zwk*1;
        }
      );
      STAT_ALL.filter((b) => ( b.MID == v.MID && v.A == b.id*1 )).forEach(
        function(c){
          A.Zwk+=c.Zwk*1;
        }
      );
    }
  );

  /*
  var startliste=STARTLISTE.reduce((a,b)=>(a.concat(b)));
    

  var Tabelle=Object.values(TEAMS).filter( (a) => ( ( a.start <= turnier.start_saison || turnier.id == 0 ) && ( startliste.indexOf( a.id*1 ) > -1 || ( turnier.id == 0 && a.id*1 > 0 ) ) ) )
  .sort(
    function(a,b){
      return a[turnier.id].P < b[turnier.id].P ||
      ( a[turnier.id].P == b[turnier.id].P && a[turnier.id].Td < b[turnier.id].Td ) ||
      ( a[turnier.id].P == b[turnier.id].P && a[turnier.id].Td == b[turnier.id].Td && a[turnier.id].Tp < b[turnier.id].Tp ) ||
      ( a[turnier.id].P == b[turnier.id].P && a[turnier.id].Td == b[turnier.id].Td && a[turnier.id].Tp == b[turnier.id].Tp && a.nt > b.nt ) ? 1 : -1;
    }
  ).filter(
    function(b){
      if( ! b[turnier.id].active && turnier.id != 0 ) return false;
      if( b[turnier.id].P == 0 && b[turnier.id].Tp == 0 && b[turnier.id].Tn == 0 ) return false;
      return true;
    } );
  */

  var Tabelle=[];
  STARTLISTE.forEach(
    function(v,i){
      Tabelle.push( Object.values(TEAMS).filter( (a) => ( ( a.start <= turnier.start_saison || turnier.id == 0 ) && ( v.indexOf( a.id*1 ) > -1 || ( turnier.id == 0 && a.id*1 > 0 ) ) ) )
      .sort(
        function(a,b){
          return a[turnier.id].P < b[turnier.id].P ||
          ( a[turnier.id].P == b[turnier.id].P && a[turnier.id].Td < b[turnier.id].Td ) ||
          ( a[turnier.id].P == b[turnier.id].P && a[turnier.id].Td == b[turnier.id].Td && a[turnier.id].Tp < b[turnier.id].Tp ) ||
          ( a[turnier.id].P == b[turnier.id].P && a[turnier.id].Td == b[turnier.id].Td && a[turnier.id].Tp == b[turnier.id].Tp && a.nt > b.nt ) ? 1 : -1;
        }
      ).filter(
        function(b){
          if( ! b[turnier.id].active && turnier.id != 0 ) return false;
          if( b[turnier.id].P == 0 && b[turnier.id].Tp == 0 && b[turnier.id].Tn == 0 ) return false;
          return true;
        } )
      );
    }
  );

  if( !show ){
    return Tabelle;
  }

  var tabelle='#tabelle_P tbody';
  $(tabelle).html('');

  //var TabelleLast= ( saison > 53 ) ? calculateAllPunktestand()[saison-53] : [];
  var TabelleLast = [];
  //var M = calculateAllPunktestand().map((a) => ( a[0].id ));
  var M = [];

  
  //STARTLISTE.forEach(
  //  function(w,j){
  //    Tabelle.filter((a) => ( w.indexOf(a.id*1) > -1 )).forEach(

  Tabelle.forEach(
    function(w,j){
      w.forEach(
        function(v,i){
          var border=Turnier.finalteams == i+1 ? 'dashed ' : '';          
          var tr = _e('tr');
          if( STARTLISTE.length > 1 ) tr.classList = 'gruppe' + j;
          
          var td = _e('td');
          td.classList = border;
          td.innerText = (i+1);
          tr.append(td);
          
          var t=v[turnier.id].Tr.map( (a) => ( a != 'X' ) ? 1 : 0 );
          var trophy=ERGEBNISSE.filter((a) => ( a.turnierID==TURNIER_ID && a.runde=='F')).map((a) => ( a.TH > a.TA ? a.H : a.A ))[0];
          trophy = trophy == undefined ? '' : trophy;
          var master=TabelleLast.length > 0 ? TabelleLast[0].id : '';
          if( t.length == 0 ) t.push(0);

          var S=t.reduce( (a,b) => a+b );
          var pokal=ERGEBNISSE.filter((a) => ( ( a.saison*1 < turnier.start_saison || turnier.id == 0 ) && a.runde=='F' && ( parseInt(a.TH) > parseInt(a.TA) && a.H*1 == v.id*1 || parseInt(a.TH) < parseInt(a.TA) && a.A*1 == v.id*1 ) ) ).length;
          var finalist=ERGEBNISSE.filter((a) => ( ( a.saison*1 < turnier.start_saison || turnier.id == 0 ) && a.runde=='F' && ( parseInt(a.TH) < parseInt(a.TA) && a.H*1 == v.id*1 || parseInt(a.TH) > parseInt(a.TA) && a.A*1 == v.id*1 ) ) ).length;
          //var meistertitel=M.filter( (a,i) => ( a == v.id*1 && ( i < saison - 26 || saison == 0 ) ) ).length;
          var meistertitel = 0;
          //var managervoted=Object.values(MANAGERVOTING_WINNER).filter((a) => (a == v.nt)).length;
          var managervoted = 0;
          var td = _e('td');
          td.classList = border;
          td.innerHTML = S;
          tr.append(td);

          var td = _e('td');
          td.classList = border;
          if( MANAGERVOTING ){
            var _i = _e('i');
            _i.classList = 'fas fa-thumbs-up ';
            $(_i).attr('onclick', "teamVoting('+v.nt+')");
            if( SAISON_TEAMVOTED.reduce((a,b) => (a.concat(b))).indexOf(v.nt) > -1 ){
              _i.classList += 'done';
              _i.title = 'Manager dieses Teams hat sein Voting abgegeben!';
            } else {
              _i.title = 'Manager dieses Teams hat noch kein Voting abgegeben!';
            }
            td.append(_i);
          }

          /*
          var img = _e('img');
          img.classList = 'flags';
          img.src = '/images/flags/'+(( v.flag ) ? v.flag : v.nt.toLowerCase())+'.svg';
          td.append(img);
          */

          var span = _e('span');
          span.innerText = ' ';
          td.append(span);

          var _a = _e('a');
          _a.href = "javascript:calculateTeam("+TURNIER_ID+","+v.id+")";
          _a.innerHTML = v.name.replace(/\(NL\)/g,'<sup>'+v.nt.toUpperCase()+'</sup> ');
          td.append(_a);
          td.append(' ')

          var _a = _e('a');
          _a.href = 'https://ultrasoccer.de/players/?id=' + v.id;
          _a.target = 'ultrasoccer';

          var _i = _e('i');
          _i.classList = 'fas fa-link';
          _a.append(_i);
          td.append(_a);

          if( trophy == v.id*1 ){
            var _i = _e('i');
            _i.classList = 'fas fa-trophy';
            _i.title = 'Amtierender Titelgewinner';
            td.append(_i);
          }

          if( master == v.id*1 ){
            var _i = _e('i');
            _i.classList = 'fas fa-crown';
            _i.title = 'Amtierender Meister';
            td.append(_i);
          }

          if(  v.id*1 == MANAGERVOTING_TEAM ){
            var _i = _e('i');
            _i.classList = 'managervoted fas fa-thumbs-up';
            _i.title = 'Team der Saison, gewählt von den Managern';
            td.append(_i);
          }
          tr.append(td);
          
          var td = _e('td');
          td.classList = border;
          td.innerText = ((pokal>0) ? pokal : '');
          tr.append(td);
          
          /*
          var td = _e('td');
          td.classList = border;
          td.innerText = ((meistertitel>0) ? meistertitel : '');
          tr.append(td);
          */

          /*
          var td = _e('td');
          td.classList = border;
          td.innerText = ((finalist>0) ? finalist : '');
          tr.append(td);
          */

          /*
          var td = _e('td');
          td.classList = border;
          td.innerText = ((managervoted>0) ? managervoted : '');
          tr.append(td);
          */
          
          var td = _e('td');
          td.classList = border;
          td.innerText = v[turnier.id].Tr.map( (a) => ( ( a == 'S') ? 1 : 0 ) ).reduce( (a,b) => a+b );
          tr.append(td);
          
          var td = _e('td');
          td.classList = border;
          td.innerText = v[turnier.id].Tr.map( (a) => ( ( a == 'U') ? 1 : 0 ) ).reduce( (a,b) => a+b );
          tr.append(td);
          
          var td = _e('td');
          td.classList = border;
          td.innerText = v[turnier.id].Tr.map( (a) => ( ( a == 'N') ? 1 : 0 ) ).reduce( (a,b) => a+b );
          tr.append(td);

          var td = _e('td');
          td.classList = border;
          td.innerHTML = v[turnier.id].Tr.map( (a) => '<'+a+'></'+a+'>' ).slice(-6).join('');
          tr.append(td);

          var td = _e('td');
          td.classList = border;
          td.innerText = v[turnier.id].Tp+'/'+v[turnier.id].Tn;
          tr.append(td);
          
          var td = _e('td');
          td.classList = border + ' ' + ((v[turnier.id].Td > 0 ) ? 'p' : (v[turnier.id].Td == 0 ) ? 'u' : 'n');
          td.innerText = ((v[turnier.id].Td > 0 ) ? '+' : '') + v[turnier.id].Td;
          tr.append(td);
          
          var td = _e('td');
          td.classList = border + ' ' + ((v[turnier.id].Zwk > 0 ) ? 'p' : (v[turnier.id].Zwk == 0 ) ? 'u' : 'n');
          td.innerText = (v[turnier.id].Zwk > 0 ? '+' : '')+(v[turnier.id].Zwk / S).toFixed(2);
          tr.append(td);
          
          var td = _e('td');
          td.classList = border;
          td.innerText = v[turnier.id].P;
          tr.append(td);
          $(tabelle).append(tr);
        }
      );
    }
  );
}

function calculateTabelle(saison,param){
  var STATISTIK=saison == 0 ? STATISTIK_ALL : STATISTIK_SAISON;
  var n=Object.entries( STATISTIK ).sort( (a,b) => ( a[1][param] < b[1][param] || ( a[1][param] == b[1][param] && a[1].S > b[1].S ) ) ? 1 : -1 ).slice(0,20).map(
    function(v,i){
      
      var team=TEAMS.filter( (a) => ( a.id*1 == v[1].teamId*1 ) );

      var tr=document.createElement('tr');

      if( team.length == 0 ){
        /*
        var flag = document.createElement('img');
        flag.classList='flags';
        flag.src='./images/flags/xx.svg'
        flag.title='n/a';
        */
        var span = document.createElement('span');
        span.innerText='';

      } else {

        var face = document.createElement('td');
        face.classList='face';

        if( v[1].face.split(/_/g).length == 5 ){
          var img = document.createElement('img');
          img.src='images/facegenerator/fg_head_' + v[1].face.split(/_/g)[0] + '.png';
          $(face).append(img);

          var img = document.createElement('img');
          img.src='images/facegenerator/fg_hair_' + v[1].face.split(/_/g)[1] + '.png';
          $(face).append(img);

          var img = document.createElement('img');
          img.src='images/facegenerator/fg_beard_' + v[1].face.split(/_/g)[2] + '.png';
          $(face).append(img);

          var img = document.createElement('img');
          img.src='images/facegenerator/fg_mouth_' + v[1].face.split(/_/g)[3] + '.png';
          $(face).append(img);

          var img = document.createElement('img');
          img.src='images/facegenerator/fg_eyes_' + v[1].face.split(/_/g)[4] + '.png';
          $(face).append(img);

        }

        var flag = document.createElement('img');
        flag.classList='flags';
        flag.src='./images/flags/'+v[1].nt+'.svg';
        //flag.src='./images/flags/xx.svg';
        //flag.title=team[0].name.split('(NL)')[0].trim();
        
        var span = document.createElement('span');
        var a_ = document.createElement('a');
        a_.href='javascript:calculateTeam('+saison+','+team[0].id+')';
        a_.innerHTML=team[0].name.replace(/\(NL\)/g,'<sup>'+team[0].nt.toUpperCase()+'</sup> ');
        span.append(a_);

        var a_ = document.createElement('a');
        a_.href=team[0].link;
        a_.target='ultrasoccer';
        var i_ = document.createElement('i');
        i_.classList='fas fa-link';
        a_.append(i_);
        span.append(a_);
      }

      var attr=document.createElement('span');
      attr.innerHTML=((v[1].attr != undefined) ? v[1].attr : '').split(' ').map( (a) => ( a != '' && a != 'fas' ? '<i class="fas '+a+'"></i>' : '' ) ).join('');

      var td=document.createElement('td');
      td.innerText=(i+1);
      tr.append(td);

      var td=document.createElement('td');
      td.innerText=(( STATISTIK[v[0]] != undefined ) ? STATISTIK[v[0]].S : 0);
      tr.append(td);

      tr.append(face);

      var td=document.createElement('td');
      td.append(flag);
      td.append(attr);

      var a_=document.createElement('a');
      a_.href='https://ultrasoccer.de/playerprofile/?id='+v[0];
      a_.target='ultrasoccer';
      a_.innerText=(( v[1].name != null ) ? v[1].name+' ('+(v[1].age*1+((saison == 0 ) ? Turnier.start_saison : saison )*1-v[1].age_saison)+')' : v[0] );
      td.append(a_);
      tr.append(td);

      var td=document.createElement('td');
      td.append(span)
      tr.append(td);

      var td=document.createElement('td');
      td.innerText = ( param == 'Z' &&  v[1][param] > 0 ) ? '+' + v[1][param] :  v[1][param];
      if( param == 'Z' ) td.classList = v[1][param] > 0 ? 'p' : ( v[1][param] == 0 ) ? 'u' : 'n';
      tr.append(td);

      return tr;

    }
  );

  $('#tabelle_'+param+' tbody').html( n );
  $('.news').toggleClass('d-none', false);
}

function calculateTeam(saison,teamID){
  var STATISTIK=saison == 0 ? STATISTIK_ALL : STATISTIK_SAISON;

  var n=SPIELER.filter( (a) => ( a.teamID*1 == teamID ) ).map( (b) => ( [ b.id, STATISTIK[b.id] ] ) ).filter( (c) => ( c[1] !== undefined ) ).map(
    function(v,i){

      var team=TEAMS.filter( (a) => ( a.id*1 == v[1].teamId*1 ) );

      var tr=document.createElement('tr');

      if( team.length == 0 ){
        /*
        var flag = document.createElement('img');
        flag.classList='flags';
        flag.src='./images/flags/xx.svg'
        flag.title='n/a';
        */
        var span = document.createElement('span');
        span.innerText='';

      } else {

        var face = document.createElement('td');
        face.classList='face';

        if( v[1].face.split(/_/g).length == 5 ){
          var img = document.createElement('img');
          img.src='images/facegenerator/fg_head_' + v[1].face.split(/_/g)[0] + '.png';
          $(face).append(img);

          var img = document.createElement('img');
          img.src='images/facegenerator/fg_hair_' + v[1].face.split(/_/g)[1] + '.png';
          $(face).append(img);

          var img = document.createElement('img');
          img.src='images/facegenerator/fg_beard_' + v[1].face.split(/_/g)[2] + '.png';
          $(face).append(img);

          var img = document.createElement('img');
          img.src='images/facegenerator/fg_mouth_' + v[1].face.split(/_/g)[3] + '.png';
          $(face).append(img);

          var img = document.createElement('img');
          img.src='images/facegenerator/fg_eyes_' + v[1].face.split(/_/g)[4] + '.png';
          $(face).append(img);
        } else {
          face.innerHTML = '&nbsp;';
        }

        tr.append(face);

        
        
        var flag = document.createElement('img');
        flag.classList='flags';
        flag.src='./images/flags/'+v[1].nt+'.svg';
        flag.title=team[0].name.split('(NL)')[0].trim();
        

        var span = document.createElement('span');
        var a_ = document.createElement('a');
        a_.href='javascript:calculateTeam('+saison+','+team[0].id+')';
        a_.innerHTML=team[0].name.replace(/\(NL\)/g,'<sup>'+team[0].nt.toUpperCase()+'</sup> ');
        span.append(a_);

        var a_ = document.createElement('a');
        a_.href=team[0].link;
        a_.target='ultrasoccer';
        var i_ = document.createElement('i');
        i_.classList='fas fa-link';
        a_.append(i_);
        span.append(a_);
      }

      var attr=document.createElement('span');
      attr.innerHTML=((v[1].attr != undefined) ? v[1].attr : '').split(' ').map( (a) => ( a != '' && a != 'fas' ? '<i class="fas '+a+'"></i>' : '' ) ).join('');

      var td=document.createElement('td');
      td.append(flag);
      tr.append(td);

      var td=document.createElement('td');
      td.append(attr);
      var a_=document.createElement('a');
      a_.href='https://ultrasoccer.de/playerprofile/?id='+v[0];
      a_.target='ultrasoccer';
      a_.innerText=(( v[1].name != null ) ? v[1].name : v[0] );
      td.append(a_);
      tr.append(td);


      var td=document.createElement('td');
      td.innerText=(v[1].age*1+((saison == 0 ) ? Turnier.start_saison : saison )*1-v[1].age_saison);
      tr.append(td);

      var td=document.createElement('td');
      td.innerText=v[1].S+'/'+v[1].T+'/'+v[1].A;
      tr.append(td);

      var td=document.createElement('td');
      td.innerText=v[1].Zpos+'/'+v[1].Zneg;
      td.classList=(( v[1].Z > 0 ) ? 'p' : (( v[1].Z < 0 ) ? 'n' : 'u' ) )
      tr.append(td);

      return tr;
    }
  );
  $('table.tabelle').toggleClass('d-none',true);
  $('#tabelle_team').toggleClass('d-none',false);
  $('#tabelle_team tbody').html( n );
}
function calculateEwige(){
  calculatePunktestand(0,'7');
  calculateTabelle(0,'T');
  calculateTabelle(0,'A');
  calculateTabelle(0,'Z');
  $('button.active').toggleClass('active',false);
  $('#ewige').toggleClass('active',true);
  $('table.tabelle').toggleClass('d-none',true);
  $('#tabelle_P').toggleClass('d-none');
  $('h4.tabelle > button.punktestand').toggleClass('active',true);
  $('h4.tabelle').toggleClass('d-none',false);
  $('.news').toggleClass('d-none', true);
}

function backToActiveTable(){
  $('table.tabelle').toggleClass('d-none',true);
  $('#tabelle_'+ $('button.active').text().toLowerCase() ).toggleClass('d-none');
}
/*
function calculateHistorie(){
  $('#tabelle_historie thead tr').html('<th>Pl.</th>');
  var T=[];
  for( var i=0; i<=SAISON_NOW-26; i++){
    $('#tabelle_historie thead tr').append('<th>'+(i+1)+'<sup>'+(i+26)+'</sup></th>');
    T.push(calculatePunktestand(i+26,7,false));
  }
  $('#tabelle_historie thead tr').append('<th class="rest"></th>');

  var masters=ERGEBNISSE.filter((a) => ( a.runde == 'F' )).map((a) => (a.TH*1 > a.TA*1 ? a.H*1 : a.A*1 ));
  var mx=T.map((a)=>(a.length)).reduce((a,b) => (Math.max(a,b)));

  var tmp='';
  for( var p=0; p<mx; p++ ){
    tmp+='<tr><td>'+(p+1)+'</td>';
    for( var i=0; i<=SAISON_NOW-26; i++){
      var teamID=T[i][p] != undefined ? T[i][p].id : undefined;
      tmp+='<td class="'+(( MANAGERVOTING_WINNER[i+26] == teamID && nt != undefined ) ? 'managervoted ' : '')+(( masters[i] == teamID && teamID != undefined ) ? 'master ' : '')+(( SAISON_SETTINGS[i] == p+1 ) ? 'dashed ' : '')+'">'+(( teamID != undefined ) ? '<img class="flags" title="'+T[i][p].name+'" src="./images/flags/'+(( T[i][p].flag ) ? T[i][p].flag : T[i][p].nt.toLowerCase())+'.svg">' : '')+'</td>';
    }
    tmp+='</tr>';
  }
  $('#tabelle_historie tbody').html(tmp);
  $('table.tabelle').toggleClass('d-none',true);
  $('#tabelle_historie').toggleClass('d-none',false);
  $('button.active').toggleClass('active',false);
  $('#historie').toggleClass('active',true);
  $('h4.tabelle').toggleClass('d-none',true);
  $('.news').toggleClass('d-none', true);
  $('.rest')[0].scrollIntoView();
}
*/

var R=undefined;

function showLastResults(H,A){
  $('#lastresults').toggleClass('d-none',false);
  R=ERGEBNISSE.filter((a) => ( a.H==H && a.A==A || a.A==H && a.H==A) ).slice(-5);
  $('#lastresults table tbody').html(R.map(
      function(c){
        var teamH=TEAMS.filter( (a) => ( a.id == c.H ) );
        teamHflag = teamH[0].flag ? teamH[0].flag : teamH[0].nt;
        var teamA=TEAMS.filter( (a) => ( a.id == c.A ) );
        teamAflag = teamA[0].flag ? teamA[0].flag : teamA[0].nt;

        var tr = _e('tr');

        var td = _e('td');
        td.innerText = c.turnierID;
        tr.append(td);

        var td = _e('td');
        td.innerText = c.runde;
        tr.append(td);

        var td = _e('td');
        /*
        var img = _e('img');
        img.classList = 'flags';
        img.src = './images/flags/'+(( c.H == H ) ? teamHflag : teamAflag )+'.svg';
        td.append(img);
        */
        tr.append(td);

        var td = _e('td');
        /*
        var img = _e('img');
        img.classList = 'flags';
        img.src = './images/flags/'+(( c.A == A ) ? teamAflag : teamHflag )+'.svg';
        td.append(img);
        */
        var span = _e('span');
        span.innerHTML = teamH[0].name;
        td.append(span);
        td.append(' - ');

        var span = _e('span');
        span.innerHTML = teamA[0].name;
        td.append(span);

        tr.append(td);

        var td = _e('td');
        var _a = _e('a');
        _a.href = 'https://ultrasoccer.de/match/?id='+c.MID;
        _a.target = 'ultrasoccer';
        _a.innerText = (( c.H == H ) ? c.TH : c.TA );
        td.append(_a);
        tr.append(td);

        var td = _e('td');
        td.innerText = ':';
        tr.append(td);

        var td = _e('td');
        var _a = _e('a');
        _a.href = 'https://ultrasoccer.de/match/?id='+c.MID;
        _a.target = 'ultrasoccer';
        _a.innerText = (( c.A == A ) ? c.TA : c.TH );
        td.append(_a);
        tr.append(td);

        return tr;
      }
    )
  );

  if( R.length > 0 ){
    var d=R.map(
      (a) =>
      (
        { 'TH' : (( a.H == H ) ? parseInt(a.TH) : parseInt(a.TA)),
          'TA' : (( a.A == A ) ? parseInt(a.TA) : parseInt(a.TH)),
          'S' : (( a.H == H ) ? parseInt(a.TH) > parseInt(a.TA) : parseInt(a.TA) > parseInt(a.TH)) ? 1 : 0,
          'U' : parseInt(a.TH) == parseInt(a.TA) ? 1 : 0,
          'N' : (( a.H == H ) ? parseInt(a.TH) < parseInt(a.TA) : parseInt(a.TA) < parseInt(a.TH)) ? 1 : 0
        }
      )
    ).reduce(
      (sum,val) =>
      (
        sum.TH += val.TH,
        sum.TA += val.TA,
        sum.S  += val.S,
        sum.U  += val.U,
        sum.N  += val.N,
        sum
      )
    );
  } else {
    var d={TH: H, TA: A,S:0, U:0, N: 0};
  }
  

  var tr = _e('tr');
  tr.classList = 'lastresults_summe';

  var td = _e('td');
  td.colspan = 4;
  td.innerText = 'Tore';
  tr.append(td);

  var td = _e('td');
  td.innerText = d.TH;
  tr.append(td);

  var td = _e('td');
  td.innerText = ':';
  tr.append(td);

  var td = _e('td');
  td.innerText = d.TA;
  tr.append(td);

  $('#lastresults table tbody').append(tr);

  var tr = _e('tr');
  tr.classList = 'lastresults_summe';

  var td = _e('td');
  td.colspan = 4;
  td.innerText = 'S/U/N';
  tr.append(td);

  var td = _e('td');
  td.innerText = d.S;
  tr.append(td);

  var td = _e('td');
  td.innerText = d.U;
  tr.append(td);

  var td = _e('td');
  td.innerText = d.N;
  tr.append(td);

  $('#lastresults table tbody').append(tr);

}

var ignore_hidelastresults=false;

function hideLastResults(force=false){
  if( !ignore_hidelastresults || force ) $('#lastresults').toggleClass('d-none',true);
  if( force ) ignore_hidelastresults=false;
}

function ignoreHideLastResults(){
  ignore_hidelastresults=true;
}


/*
function calculateAllPunktestand(){

  var T=[];
  for( var i=0; i<=SAISON_NOW-26; i++){
    if( ERGEBNISSE.filter((a) => ( a.runde=='F' && a.saison==i+26 )).length == 1 ){
      T.push( JSON.parse(JSON.stringify( calculatePunktestand(i+26,7,false) )) );
    }
  }
  return T;
}
*/

function teamVoting(teamID){
  var team=TEAMS.filter((a) => (a.id == teamID && a.ende == 0))[0];
  var cd=prompt('TeamVote für \n\n' + team.name + '\n\n -> Bitte AuthCode eingeben!');
  $.ajax({
    'type':"POST",
    'async': false,
    'url': 'vote.php',
    'data' : { 'teamvote' : teamID, 'secret' : cd },
    'dataType': 'json',
    'success': function (data) {
        json = data;
    },
    'error': function (data) {
      json = data;
    }
  });
  if( json[3] ){
    alert( 'Teamvoting erfolgreich gesetzt!' );
  } else {
    console.log( json );
  }
}