<?php

class db
{
  private $servername = '';
  private $username = '';
  private $password = '';
  private $aes_key = '';
  private $dbname = '';
  public $conn;

  function __construct() {
    // Create connection
    $this->servername = getenv('MYSQL_HOST').':'.getenv('MYSQL_PORT');
    $this->username = getenv('MYSQL_USER');
    $this->password = getenv('MYSQL_PASSWORD');
    $this->aes_key = getenv('AES_KEY');
    $this->dbname = getenv('MYSQL_DATABASE');

    $this->conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
    // Check connection
    if ($this->conn->connect_error) {
       die('Connection failed: ' . $this->conn->connect_error);
    }
  }

  function __destruct() {
    $this->conn->close();
  }

  public function query($sql){
    $sql=preg_replace('/KEY/','"'.$this->aes_key.'"',$sql);
    return $this->conn->query($sql);
  }

  public function commit(){
    return $this->conn->commit();
  }

  public function autocommit($bool){
    return $this->conn->autocommit($bool);
  }
}

?>
