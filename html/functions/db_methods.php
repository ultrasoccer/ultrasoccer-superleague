<?php

function db_addErgebnisse( $csv, $turnierID ){

  $C=preg_split('/,/',$csv);
  $C[7] = trim($C[7]);

  if( !is_numeric($C[6]) || !is_numeric($C[7]) ){
    return db_removeErgebnisse( $turnierID.','.$csv );
  }

  array_unshift($C, $turnierID );
  array_pop( $C );
  
  $C[1]='"'.$C[1].'"';
  $C[3]='"'.$C[3].'"';
  //$C[4]='"'.$C[4].'"';
  //$C[5]='"'.$C[5].'"';
  
  //$user=db_checkSecret($secret);
  $user = db_getUserByIp();
  if( empty( $user ) ) return false;
  

  $C[]=$user['id'];

  $csv=implode(',',$C);

  $db=new db;
  $sql = "REPLACE INTO Ergebnisse VALUES ( ".$csv." )";

  if ( $db->query( $sql ) === TRUE) {
      // success
      $result=array( 'success' => true, 'message' => 'Spieltag erfolgreich hinzugefügt!', 'data' => [] );
  } else {
      echo 'SQL-Error: '.$sql.'<br>';
  }

  unset($db);
  return $result;
}
function db_addSpieler( $csv, $secret ){

  //CSV: SpielerID,Spieler,Alter,Nt,Saison,Liga,TeamID,Team,S,T,A,Z+,Z-,Zd,Bk,Bi+,Bi-,Attr,Face,Karriereende

  // ba79796eb0452460503db15aefe3c332,Maximilian Koller,20,at,36,4-3,1496,Verein Bottrop,4,3,0,9,5,4,7,12,4,,6_9_7_1_5,0
  // ba79796eb0452460503db15aefe3c332,Maximilian Koller,20,at,36,3-3,1613,SV Saint-Pierre,1,0,0,0,0,0,6,7,0,,6_9_7_1_5,1

  //DB: teamID,nt,id,attr,name,age,age_saison,face,userID,zeitstempel

  $C=preg_split('/,/',$csv);

  $TEAMS=db_getIdNtFromTeams();
  if( $TEAMS[$C[6]] != $C[6] ) return false;

  $D=[];
  $D[]=$C[6];  // teamID
  $D[]='"'.$C[3].'"';  // nt
  $D[]='0x'.$C[0];  // id
  $D[]='"'.$C[17].'"'; // attr
  $D[]='"'.$C[1].'"';  // name
  $D[]=$C[2];  // age
  $D[]=$C[4];  // age_saison
  $D[]='"'.$C[18].'"'; // face


  /*
  $C[1]='"'.$C[1].'"';
  $C[2]='0x'.$C[2];
  $C[3]='"'.$C[3].'"';
  $C[4]='"'.$C[4].'"';
  unset($C[6]);
  unset($C[7]);
  unset($C[8]);
  unset($C[9]);
  unset($C[10]);
  unset($C[11]);
  unset($C[12]);
  unset($C[13]);
  */

  //$C[]=SAISON_NOW;
  //$user=db_checkSecret($secret);
  $user = db_getUserByIp();
  if( empty( $user ) ) return false;

  $D[]=$user['id'];

  if( $S=db_getFromSpielerById($D[2]) ){
    $D[]='"'.$S['zeitstempel'].'"';
  } else {
    $D[]='NOW()';
  }

  $csv=implode(',',$D);

  $db=new db;

  $sql = "REPLACE INTO Spieler VALUES ( ".$csv." )";

  if ( $db->query( $sql ) === TRUE) {
      // success
      $result=array( 'success' => true, 'message' => 'Spieler erfolgreich hinzugefügt!', 'data' => [] );
  } else {
      echo 'SQL-Error: '.$sql.'<br>';
  }

  unset($db);
  return $result;
}
function db_addStatistik( $csv, $secret ){

  $C=preg_split('/,/',$csv);
  if( $C[5] == '0' ) return;
  $C[4]='"'.$C[4].'"';
  $C[6]='0x'.$C[6];
  unset($C[0]);
  unset($C[1]);
  unset($C[7]);

  //$user=db_checkSecret($secret);
  $user = db_getUserByIp();
  if( empty( $user ) ) return false;

  $ergebnis=db_getFromErgebnisseByMID($C[2]);
  if( empty( $ergebnis ) || $C[2] == '0' ) return;

  $C[]=$user['id'];

  $C[]='NOW()';

  $csv=implode(',',$C);

  $db=new db;

  $sql = "REPLACE INTO Statistik VALUES ( ".$csv." )";

  if ( $db->query( $sql ) === TRUE) {
      // success
      $result=array( 'success' => true, 'message' => 'Statistik erfolgreich hinzugefügt!', 'data' => [] );
  } else {
      echo 'SQL-Error: '.$sql.'<br>';
  }

  unset($db);
  return $result;
}
function db_addTeamVote( $secret, $saison, $teamvote ){

  $teams=db_checkTeamSecret($secret);
  if( empty( $teams ) ) return false;
  if( empty( $teams[0]['manager'] ) ) return false;
  foreach( $teams as $team ){
    if( $team['nt'] == $teamvote ) return false;
  }

  $manager=$teams[0]['manager'];

  $db=new db;

  $sql = "REPLACE INTO Voting ( manager, saison, teamvote ) VALUES ( '".$manager."',".$saison.",'".$teamvote."' )";

  if ( $db->query( $sql ) === TRUE) {
      // success
      $result=array( 'success' => true, 'message' => 'Spieler erfolgreich hinzugefügt!', 'data' => [] );
  } else {
      echo 'SQL-Error: '.$sql.'<br>';
  }

  unset($db);
  return $result;
}

function db_addTeam( $id, $team, $manager, $nt, $start ){

  //$teams=db_checkTeamSecret($secret);
  //if( empty( $teams ) ) return false;
  //if( empty( $teams[0]['manager'] ) ) return false;
  //foreach( $teams as $team ){
  //  if( $team['nt'] == $teamvote ) return false;
  //}

  //$manager=$teams[0]['manager'];

  $db=new db;

  $sql = "REPLACE INTO Teams ( id, name, manager, nt, start ) VALUES ( ".$id.",'".$team."','".$manager."', '".$nt."', ".$start." )";

  if ( $db->query( $sql ) === TRUE) {
      // success
      $result=array( 'success' => true, 'message' => 'Team erfolgreich hinzugefügt/angepasst!', 'data' => [] );
  } else {
      echo 'SQL-Error: '.$sql.'<br>';
  }

  unset($db);
  return $result;
}

function db_addTurnier( $id, $turnier, $start_datum, $start_saison, $start_spieltag, $partien_abstand, $finalteams, $finale_abstand ){

  $db=new db;

  if( $id == 'neu' ) $id = 0;
  $sql = "REPLACE INTO Turnier ( id, name, start_datum, start_saison, start_spieltag, partien_abstand, finalteams, finale_abstand ) VALUES ( ".$id.",'".$turnier."','".$start_datum."', ".$start_saison.", ".$start_spieltag.", '".$partien_abstand."', '".$finalteams."', '".$finale_abstand."' )";

  if ( $db->query( $sql ) === TRUE) {
      // success
      $result=array( 'success' => true, 'message' => 'Turnier erfolgreich hinzugefügt/angepasst!', 'data' => [] );
  } else {
      echo 'SQL-Error: '.$sql.'<br>';
  }
  $ID = $db->conn->insert_id;
  unset($db);
  return $ID;
}

function db_addTurnier_teams( $turnierID, $teamID, $start_saison, $gruppe ){

  $db=new db;

  $sql = "REPLACE INTO Turnier_teams ( turnierID, teamID, start_saison, gruppe ) VALUES ( ".$turnierID.",".$teamID.",".$start_saison.",".$gruppe." )";

  if ( $db->query( $sql ) === TRUE) {
      // success
      $result=array( 'success' => true, 'message' => 'Team erfolgreich zugeordnet/angepasst!', 'data' => [] );
  } else {
      echo 'SQL-Error: '.$sql.'<br>';
  }

  unset($db);
  return $result;
}

function db_removeErgebnisse( $csv ){

  $C=preg_split('/,/',$csv);
  //$user=db_checkSecret($secret);
  $user = db_getUserByIp();
  if( empty( $user ) ) return false;

  $db=new db;
  $sql = "DELETE FROM Ergebnisse WHERE
          turnierID=".$C[0]."
          AND runde='".$C[1]."'
          AND spieltag=".$C[2]."
          AND H='".$C[4]."'
          AND A='".$C[5]."'";

  if ( $db->query( $sql ) === TRUE) {
      // success
      $result=array( 'success' => true, 'message' => 'Ergebnis erfolgreich entfernt!', 'data' => [] );
  } else {
      echo 'SQL-Error: '.$sql.'<br>';
  }

  unset($db);
  return $result;
}

function db_checkSecret($secret){

  $db=new db;

  $sql = 'SELECT * FROM User WHERE secret="'.$secret.'"';

  $result = $db->query($sql);

  $row = $result->fetch_assoc();

  unset($db);
  return $row;

}
function db_checkTeamSecret($secret){

  $db=new db;

  $sql = 'SELECT * FROM Teams WHERE secret="'.$secret.'" AND ende = 0';

  $result = $db->query($sql);

  $ROW=Array();

  while( $row = $result->fetch_assoc() ){
    $ROW[]=$row;
  }

  unset($db);
  return $ROW;

}
function db_removeUserExpiredIp(){

  $db=new db;
  $sql = "UPDATE User SET ip = '' WHERE TIMESTAMPDIFF(SECOND, `ip_timestamp`, NOW()) >= 3600";

  if ( $db->query( $sql ) === TRUE) {
      // success
      $result=array( 'success' => true, 'message' => 'Aktion erfolgreich!', 'data' => [] );
  } else {
      echo 'SQL-Error: '.$sql.'<br>';
  }

  unset($db);
  return $result;
}
function db_updateUserIpBySecret2( $secret2 ){

  $db=new db;

  $sql = "UPDATE User SET ip = '".$_SERVER['REMOTE_ADDR']."', ip_timestamp = NOW() WHERE secret2 = '".$secret2."'";

  if ( $db->query( $sql ) === TRUE) {
      // success
      $result=array( 'success' => true, 'message' => 'User erkannt, IP updated !', 'data' => [ $_SERVER['REMOTE_ADDR'] ] );
  } else {
      echo 'SQL-Error: '.$sql.'<br>';
  }

  unset($db);
  return $result;
}
function db_getUserByIp(){
  $db=new db;
  $sql='SELECT * FROM `User` WHERE ip="'.$_SERVER['REMOTE_ADDR'].'"';
  $result = $db->query($sql);
  $row = $result->fetch_assoc();
  unset($db);
  return $row;
}
function db_getFromTeams(){

  $db=new db;

  $sql = 'SELECT * FROM Teams';
  $result = $db->query($sql);

  $ROW=Array();

  while( $row = $result->fetch_assoc() ){
    $ROW[]=$row;
  }

  unset($db);
  return $ROW;

}
function db_getFromTurnier(){

  $db=new db;

  $sql = 'SELECT * FROM Turnier';
  $result = $db->query($sql);

  $ROW=Array();

  while( $row = $result->fetch_assoc() ){
    $ROW[]=$row;
  }

  unset($db);
  return $ROW;

}
function db_getFromTurnier_teams($turnierID=False){

  $db=new db;

  if( $turnierID > 0 ){
    $sql = 'SELECT * FROM Turnier_teams WHERE turnierID = '.$turnierID;
  } else {
    $sql = 'SELECT * FROM Turnier_teams ORDER BY turnierID DESC';
  }
  
  $result = $db->query($sql);

  $ROW=Array();

  while( $row = $result->fetch_assoc() ){
    $ROW[]=$row;
  }

  unset($db);
  return $ROW;

}
function db_getLastTurnier(){

  $db=new db;

  $sql = 'SELECT turnierID FROM Turnier_teams ORDER BY turnierID DESC LIMIT 1';
  $result = $db->query($sql);
  $row = $result->fetch_assoc();
  unset($db);
  return $row['turnierID'];

}
function db_getFromTeamsById($teamID){

  $db=new db;

  $sql = 'SELECT * FROM Teams WHERE id='.$teamID;
  $result = $db->query($sql);

  $row = $result->fetch_assoc();

  unset($db);
  return $row;

}
function db_getFromTeamsByManager($manager){

  $db=new db;

  $sql = 'SELECT nt FROM Teams WHERE manager="'.$manager.'"';
  $result = $db->query($sql);

  $ROW=Array();

  while( $row = $result->fetch_assoc() ){
    $ROW[]=$row['nt'];
  }

  unset($db);
  return $ROW;

}

function db_getIdNtFromTeams(){

  $db=new db;

  $sql = 'SELECT * FROM Teams';
  $result = $db->query($sql);

  $ROW=Array();

  while( $row = $result->fetch_assoc() ){
    $ROW[$row['id']]=$row['id'];
  }

  unset($db);
  return $ROW;

}
function db_getFromSpieler(){

  $db=new db;

  $sql = 'SELECT * FROM Spieler';
  $result = $db->query($sql);

  $ROW=Array();

  while( $row = $result->fetch_assoc() ){
    $row['id']=bin2hex($row['id']);
    $ROW[]=$row;
  }

  unset($db);
  return $ROW;

}
function db_getFromSpielerById($id){

  $db=new db;

  $sql = 'SELECT * FROM Spieler WHERE id='.$id;

  $result = $db->query($sql);

  if( $row = $result->fetch_assoc() ){
    $row['id']=bin2hex($row['id']);
  } else {
    return false;
  }

  unset($db);
  return $row;

}
function db_getLatestFromSpieler($days=34){

  $db=new db;

  $sql = 'SELECT Spieler.*, Statistik.zeitstempel AS zeitstempl
          FROM Spieler INNER JOIN Statistik ON Spieler.id = Statistik.SpielerID
          WHERE Spieler.zeitstempel > TIMESTAMPADD( DAY,-'.$days.',NOW() )
          GROUP BY Statistik.SpielerID
          ORDER BY zeitstempl DESC';
  $result = $db->query($sql);

  $ROW=Array();
  while( $row = $result->fetch_assoc() ){
    $row['SpielerID']=bin2hex($row['id']);
    unset($row['id']);
    //$row['SpielerID']=bin2hex($row['SpielerID']);
    $ROW[]=$row;
  }

  unset($db);
  return $ROW;

}
function db_getFromErgebnisse(){

  $db=new db;

  $sql = 'SELECT * FROM Ergebnisse';
  $result = $db->query($sql);

  $ROW=Array();

  while( $row = $result->fetch_assoc() ){
    $ROW[]=$row;
  }

  unset($db);
  return $ROW;

}
function db_getFromErgebnisseByMID($mid){

  $db=new db;

  $sql = 'SELECT * FROM Ergebnisse WHERE MID='.$mid;
  $result = $db->query($sql);

  $ROW=Array();

  while( $row = $result->fetch_assoc() ){
    $ROW[]=$row;
  }

  unset($db);
  return $ROW;

}
function db_getFromStatistikBySaison($saison){

  $db=new db;

  /*
  $sql = 'SELECT Statistik.MID, Statistik.SpielerID, Spieler.name, Spieler.nt, Spieler.age, Teams.id, SUM(Statistik.Tore) AS "Tore", SUM(Statistik.Assists) AS "Assists", SUM(Statistik.Zpos - Statistik.Zneg) AS "Zwkbilanz" FROM Statistik
          INNER JOIN Ergebnisse ON Ergebnisse.MID = Statistik.MID
          LEFT JOIN Spieler     ON Spieler.id = Statistik.SpielerID
          LEFT JOIN Teams       ON Teams.id = Spieler.teamID
          WHERE Ergebnisse.saison = '.$saison.'
          GROUP BY SpielerID, MID';
  */
  $sql='SELECT Statistik.MID, Statistik.SpielerID, Spieler.name, Spieler.nt, Spieler.attr, Spieler.age, Spieler.age_saison, Teams.id, Statistik.Tore, Statistik.Assists, Statistik.Zpos, Statistik.Zneg, Statistik.Zpos - Statistik.Zneg AS "Zwk", Spieler.face FROM Statistik
          INNER JOIN Ergebnisse ON Ergebnisse.MID = Statistik.MID
          LEFT JOIN Spieler     ON Spieler.id = Statistik.SpielerID
          LEFT JOIN Teams       ON Teams.id = Spieler.teamID
          WHERE Ergebnisse.saison = '.$saison.'
          GROUP BY SpielerID, MID';

  $result = $db->query($sql);

  $ROW=Array();

  while( $row = $result->fetch_assoc() ){
    $row['SpielerID']=bin2hex($row['SpielerID']);
    $ROW[]=$row;
  }

  unset($db);
  return $ROW;

}
function db_getFromStatistikByTurnier($turnier){

  $db=new db;

  /*
  $sql = 'SELECT Statistik.MID, Statistik.SpielerID, Spieler.name, Spieler.nt, Spieler.age, Teams.id, SUM(Statistik.Tore) AS "Tore", SUM(Statistik.Assists) AS "Assists", SUM(Statistik.Zpos - Statistik.Zneg) AS "Zwkbilanz" FROM Statistik
          INNER JOIN Ergebnisse ON Ergebnisse.MID = Statistik.MID
          LEFT JOIN Spieler     ON Spieler.id = Statistik.SpielerID
          LEFT JOIN Teams       ON Teams.id = Spieler.teamID
          WHERE Ergebnisse.saison = '.$saison.'
          GROUP BY SpielerID, MID';
  */
  $sql='SELECT Statistik.MID, Statistik.SpielerID, Spieler.name, Spieler.nt, Spieler.attr, Spieler.age, Spieler.age_saison, Teams.id, Statistik.Tore, Statistik.Assists, Statistik.Zpos, Statistik.Zneg, Statistik.Zpos - Statistik.Zneg AS "Zwk", Spieler.face FROM Statistik
          INNER JOIN Ergebnisse ON Ergebnisse.MID = Statistik.MID
          LEFT JOIN Spieler     ON Spieler.id = Statistik.SpielerID
          LEFT JOIN Teams       ON Teams.id = Spieler.teamID
          WHERE Ergebnisse.turnierID = '.$turnier.'
          GROUP BY SpielerID, MID';

  $result = $db->query($sql);

  $ROW=Array();

  while( $row = $result->fetch_assoc() ){
    $row['SpielerID']=bin2hex($row['SpielerID']);
    $ROW[]=$row;
  }

  unset($db);
  return $ROW;

}

function db_getFromStatistik(){

  $db=new db;
  $sql='SELECT Statistik.MID, Statistik.SpielerID, Spieler.name, Spieler.nt, Spieler.attr, Spieler.age, Spieler.age_saison, Teams.id, Statistik.Tore, Statistik.Assists, Statistik.Zpos, Statistik.Zneg, Statistik.Zpos - Statistik.Zneg AS "Zwk", Spieler.face FROM Statistik
          INNER JOIN Ergebnisse ON Ergebnisse.MID = Statistik.MID
          LEFT JOIN Spieler     ON Spieler.id = Statistik.SpielerID
          LEFT JOIN Teams       ON Teams.id = Spieler.teamID
          GROUP BY SpielerID, MID';

  $result = $db->query($sql);

  $ROW=Array();

  while( $row = $result->fetch_assoc() ){
    $row['SpielerID']=bin2hex($row['SpielerID']);
    $ROW[]=$row;
  }

  unset($db);
  return $ROW;

}

function db_getUnknownSpieler($teamID){

  $db=new db;
  $sql='SELECT Statistik.SpielerID as "SpielerID", Statistik.teamID as "teamID", Statistik.zeitstempel as "zeitstempel" FROM Statistik
          LEFT JOIN Spieler ON Spieler.ID = Statistik.SpielerID
          WHERE Statistik.teamID='.$teamID.' AND Spieler.Name IS NULL
          GROUP BY Statistik.SpielerID';

  $result = $db->query($sql);

  $ROW=Array();

  while( $row = $result->fetch_assoc() ){
    $row['SpielerID']=bin2hex($row['SpielerID']);
    $t=db_getFromTeamsById($teamID);
    $row['team']=$t['name'];
    $row['nt']=$t['nt'];
    //$row['flag']=$t['flag'];
    $ROW[]=$row;
  }

  unset($db);
  return $ROW;

}

function db_getActiveManager($saison){

  $db=new db;
  $sql='SELECT manager FROM Teams WHERE start <= '.$saison.' AND ende = 0 AND manager != "Dummy" GROUP BY manager';

  $result = $db->query($sql);

  $ROW=Array();

  while( $row = $result->fetch_assoc() ){
    $ROW[]=$row;
  }

  unset($db);
  return $ROW;

}

function db_getTeamvotedManagerBySaison($saison){
  $db=new db;
  $sql='SELECT manager FROM `Voting` WHERE saison='.$saison.' GROUP BY manager; ';

  $result = $db->query($sql);

  $ROW=Array();

  while( $row = $result->fetch_assoc() ){
    $ROW[]=db_getFromTeamsByManager($row['manager']);
  }

  unset($db);
  return $ROW;
  
}




?>
