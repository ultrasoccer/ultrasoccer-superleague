---
layout: teams
---
<?php

  // check user logged in 
  if( empty( db_getUserByIp() ) ) exit;

  if( !empty( $_POST['edit'] ) ){ 

    if(
      $_POST['edit'] == 'turnier' &&
      !empty( $_POST['turnierID'] ) && 
      !empty( $_POST['turnier'] ) && 
      !empty( $_POST['start_datum'] ) && 
      !empty( $_POST['start_saison'] ) && 
      !empty( $_POST['start_spieltag'] ) &&
      !empty( $_POST['finalteams'] ) &&
      !empty( $_POST['finale_abstand'] ) &&
      !empty( $_POST['partien_abstand'] ) ) { 
        $last_id = db_addTurnier( ($_POST['turnierID'] == 'add' ? 0 : $_POST['turnierID']), $_POST['turnier'], $_POST['start_datum'], $_POST['start_saison'], $_POST['start_spieltag'], $_POST['partien_abstand'], $_POST['finalteams'], $_POST['finale_abstand'] );
        ?>
        <script>
        $(document).ready(
          function(){
            //self.location.href='<?= '?t='.$_POST['turnierID'] ?>';
            self.location.href='<?= '?t='.$last_id ?>';
          }
        );
        </script>
        <? 
    }

    if( 
      $_POST['edit'] == 'turnier_teams' &&
      !empty( $_POST['turnierID'] ) &&
      !empty( $_POST['teamID'] ) && 
      !empty( $_POST['startsaison'] ) &&   
      !empty( $_POST['gruppe'] ) 
      ){
        db_addTurnier_teams( $_POST['turnierID'], $_POST['teamID'], $_POST['startsaison'], $_POST['gruppe'] );
        ?>
        <script>
        $(document).ready(
          function(){
            self.location.href='<?= '?t='.$_POST['turnierID'] ?>';
           
          }
        );
        </script>
        <? 
    }
  }

  $TEAMS = db_getFromTeams();
  $T = [];
  foreach( $TEAMS as $t ){
    $T[$t['id']] = $t;
  }

  $TURNIER_TEAMS = db_getFromTurnier_teams();
  $TT = [];
  foreach( $TURNIER_TEAMS as $t ){
    $TT[$t['turnierID']][] = $t;
  }
  
?>
<style>
  select, button, input {
    border-radius: 2px;
    border: 1px solid navy;
  }
  .half {
    width: 49%;
    float: left;
    padding: 5px;
  }
  .half:first-child {
    text-align: right;
    
  }
  .full {
    width: 100%;
    text-align: center;
  }
  table {
    width: auto;
    margin-right: 0px;
    margin-left: auto;
  }
  .spielpause_hauptrunde, .spielpause_finalrunde {
    width: 20px;
  }
</style>
<script>

  var TURNIER=<?= json_encode( db_getFromTurnier() ) ?>;
  var REF_SAISON={{ site.data.global.ref_saison }};
  var REF_DATUM='{{ site.data.global.ref_datum }}';

  $(document).ready(
    function(){
      if( getQueryVariable('t') != undefined ){
        if( TURNIER.filter( (a) => (a.id == getQueryVariable('t') ) ).length > 0 ){
          $('#turnier_sel').val(getQueryVariable('t')).change();
        } else {
          $('#turnier_sel').val('add').change();
        }
      } 
    }
  );
  

  var T = <?= json_encode($T) ?>;
  var TT = <?= json_encode($TT) ?>;

  function selectTurnier(obj){
    if( obj.value == '' ){

      $('.aktion_btn').toggleClass('d-none',false);
      $('#turnier_sel').toggleClass('d-none',true);
      $('#turnierformular').toggleClass('d-none',true);
      $('.turnierselect').toggleClass('d-none',true);
      $('#absenden_btn').toggleClass('d-none',true);
      return;
    }
    $('.aktion_btn').toggleClass('d-none',true);
    if( obj.value == 'add' ){
      $('.turnierselect').toggleClass('d-none',true);
      $('#turnierID').val('neu');
    } else {
      var Turnier = TURNIER.filter((a) => (a.id == obj.value))[0];
      Turnier.partien_abstand = isJSON(Turnier.partien_abstand) ? JSON.parse( Turnier.partien_abstand ) : Turnier.partien_abstand;
      Turnier.finale_abstand = isJSON(Turnier.finale_abstand) ? JSON.parse( Turnier.finale_abstand ) : Turnier.finale_abstand;
      $('.turnierselect').toggleClass('d-none',false);
      $('#turnierID').val( Turnier.id );
      $('#turnier').val( Turnier.name );
      $('#link').html('index.php?t='+Turnier.id).attr('href','index.php?t='+Turnier.id);
      $('#start_datum').val( Turnier.start_datum );
      $('#start_saison').val( Turnier.start_saison );
      $('#start_spieltag').val( Turnier.start_spieltag );
      $('#finalteams').val( Turnier.finalteams );
      $('#turnier_teams').html( TT[Turnier.id] ? TT[Turnier.id].sort((a,b) => (a.gruppe - b.gruppe )).map((a) => ( '<option>Gr' + a.gruppe + ' - ' + T[a.teamID].name + '</option>')).join('') : '' );
      
      var t = TT[Turnier.id]?.sort((a,b) => (a.gruppe - b.gruppe )).filter((b) => ( b.gruppe == 1 )).length;
      var spielt = Turnier.start_spieltag*1;
      if( t == undefined ) {
        $('#partien').html('');
      } else {
        $('#partien').html( STARTLISTE_PARTIEN_DB[t]?.map(
            function(a,j){
              var b = '';
              for( i = 1; i < a.length; i+=2 ){
                if( b != '' ) b += ' / ';
                b += a[i]+' vs '+a[i+1];
              }
              var res = '<small>' +b+ '</small> | ' + pad( spielt % 35, 2 ) + ' | <input type="text" class="spielpause_hauptrunde" value="'+( Turnier.partien_abstand[j] ? Turnier.partien_abstand[j]*1 : 1 ) +'">';
              spielt += ( Turnier.partien_abstand[j] ? Turnier.partien_abstand[j]*1 : 1 );
              return res;
            }
          ).join('<br>')
        );
        if( Turnier.finalteams*1 == 2 ){
          $('#partien').append('<br>F | ' + pad( spielt % 35, 2 ) + ' | <input type="text" class="spielpause_finalrunde" value="'+( Turnier.finale_abstand[0] ? Turnier.finale_abstand[0]*1 : 1 ) +'">');
        } else if( Turnier.finalteams*1 == 4 ){
          $('#partien').append('<br>HF | ' + pad( spielt % 35, 2 ) + ' | <input type="text" class="spielpause_finalrunde" value="'+( Turnier.finale_abstand[0] ? Turnier.finale_abstand[0]*1 : 1 ) +'"><input type="text" class="spielpause_finalrunde" value="'+( Turnier.finale_abstand[0] ? Turnier.finale_abstand[0]*1 : 1 ) +'">');
          spielt += ( Turnier.finale_abstand[0] ? Turnier.finale_abstand[0]*1 : 1 );
          $('#partien').append('<br>F | ' + pad( spielt % 35, 2 ) + ' | &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
        }
        
      }
      $('#turnier_sel').toggleClass('d-none',false);
      $('#turnierformular').toggleClass('d-none',false);
      $('#absenden_btn').toggleClass('d-none',false);
    } 
  }

  function neuesTurnierErstellen(){
    $('.aktion_btn').toggleClass('d-none',true);
    
    $('#turnierID').val('neu');
    $('#turnier').val('Unbenannt');
    var dat = new Date();
    dat.setDate( dat.getDate()+3 );
    $('#start_datum').val( dat.toJSON().slice(0,10) );
    var d = parseInt( ( dat - new Date(REF_DATUM) ) / (24*3600*1000) );
    $('#start_saison').val( parseInt( d / 35 ) + REF_SAISON );
    $('#start_spieltag').val( d % 35 ) ;
    $('#finalteams').val(4);
    $('#turnierformular').toggleClass('d-none',false);
    $('#absenden_btn').toggleClass('d-none',false);

  }

  function vorhandenesTurnierLaden(){
    $('.aktion_btn').toggleClass('d-none',true);
    $('#turnier_sel').toggleClass('d-none',false);
  }

  function subm(n){
    $('#edit').val(n);
    if( n == 'turnier_teams'){
      $('#gruppe').val( prompt('Gruppe Nr?',1) );
      $('#teamID').val( $('#team_sel').val().split(',')[0] );
      $('#startsaison').val( $('#team_sel').val().split(',').slice(-1)[0] );
    }
    $('#partien_abstand').val( JSON.stringify( Object.values( document.getElementsByClassName('spielpause_hauptrunde') ).map((a) => (a.value )) ) );
    $('#finale_abstand').val( JSON.stringify( Object.values( document.getElementsByClassName('spielpause_finalrunde') ).map((a) => (a.value )) ) );
    $('#formu').submit();

  }

</script>




<form id="formu" method="POST">
<input id="edit" name="edit" type="hidden" value="">
<div>

  <div class="full">

    <p>
      <a href="teams.php">Teams</a> | 
      <a href="turnier.php"><b>Turnier</b></a>
    </p>

    <h3>Turnier</h3>
    <button type="button" class="aktion_btn" onclick="neuesTurnierErstellen()">Neues Turnier erstellen</button>
    <button type="button" class="aktion_btn" onclick="vorhandenesTurnierLaden()">Vorhandenes Turnier laden</button>
  </div>
</div>
<div>
  <div class="half">
    
    <select id="turnier_sel" class="d-none" onchange="selectTurnier(this)">
      <option value="" selected>---</option>
      <?php
      foreach( db_getFromTurnier() as $t ){
        ?><option value="<?= $t['id'] ?>" title=""><?= $t['id'] ?> - <?= $t['name'] ?> (<?= $t['start_datum'] ?>)</option><?
      }
      ?>
    </select>

    <div>
      <table id="turnierformular" class="d-none">
        <tr>
          <td>id</td><td><input id="turnierID" name="turnierID" type="text" value="" readonly></td>
        </tr>
        <tr>
          <td>turnier</td><td><input id="turnier" name="turnier" type="text" value=""></td>
        </tr>
        <tr>
          <td>startet am</td><td><input id="start_datum" name="start_datum" type="text" value="" placeholder="yyyy-mm-dd"></td>
        </tr>
        <tr>
          <td>saison</td><td><input id="start_saison" name="start_saison" type="text" value="" placeholder=""></td>
        </tr>
        <tr>
          <td>spieltag</td><td><input id="start_spieltag" name="start_spieltag" type="text" value="" placeholder=""></td>
        </tr>
        <tr>
          <td>finalrundenteams</td><td><select id="finalteams" name="finalteams"><option value="2">2</option><option value="4">4</option><option value="8">8</option></select></td>
        </tr>
      </table>
    </div>

    <button id="absenden_btn" type="button" class="d-none" onclick="subm('turnier')">absenden</button>

    <div class="turnierselect d-none">
      <input id="partien_abstand" name="partien_abstand" type="hidden" value="">
      <input id="finale_abstand" name="finale_abstand" type="hidden" value="">
      <h3>Partien [Abstand] Spieltag</sup></h3>
      <p id="partien"></p>
    </div>
  </div>
  <div class="half">
    <div class="turnierselect d-none">
    <p> Link: <a id="link" href="" target="extra"></a></p>
    <h3>Gruppen</h3>
    <input id="gruppe" name="gruppe" type="hidden" value="0">
    <input id="teamID" name="teamID" type="hidden" value="0">
    <input id="startsaison" name="startsaison" type="hidden" value="0">
    <select id="team_sel" class="turnierselect d-none" onchange="selectTeam(this)">
    <?php
      foreach( $TEAMS as $t ){
        ?><option value="<?= $t['id'] ?>,<?= $t['name'] ?>,<?= $t['manager'] ?>,<?= $t['nt'] ?>,<?= $t['start'] ?>" title="Manager: <?= $t['manager'] ?>]"><?= $t['id'] ?>/<?= $t['start'] ?> - <?= $t['name'] ?> (<?= $t['nt'] ?>)</option><?
      }
    ?>
    </select>
    <button type="button" class="turnierselect d-none" onclick="subm('turnier_teams')">zuordnen</button>
  </div>
  <div>
    <select id="turnier_teams" class="turnierselect d-none" onchange="selectTurnierTeams(this)" multiple="multiple" style="width:400px; height:400px;">
    </select>
  </div>
  </div>
</div>

</form>

