---
layout: teams
---
<?php

  // check user logged in 
  if( empty( db_getUserByIp() ) ) exit;

  if( !empty($_POST['teamID']) && !empty($_POST['team']) && !empty($_POST['manager']) && !empty($_POST['nt']) && !empty($_POST['start']) ){
    $result = db_addTeam( $_POST['teamID'], $_POST['team'], $_POST['manager'], $_POST['nt'], $_POST['start'] );
    if($result['success'] == true){
      echo "<p>Team erfolgreich geändert/hinzugefügt</p>";
    } else {
      echo "<p>Aktion fehlgeschlagen!</p>";
    }
  }
  
?>



<style>

.full {
    width: 100%;
    text-align: center;
  }
.full table {
  margin: auto;
  text-align: right;
}
</style>

<script>
  function selectTeam(obj){
    if( obj.value == 'add' ){
    } else {
      a = obj.value.split(',');
      $('#teamID').val(a[0]);
      $('#team').val(a[1]);
      $('#manager').val(a[2]);
      $('#nt').val(a[3]);
      $('#start').val(a[4]);
    }
    $('#teamID').attr('readonly', obj.value != 'add');
    
  }

  function editTeam(){
    $('#formu').submit();
  }

  function readCsv(obj){
    var C = obj.value.split('\n');
    if( C[0] == 'TeamID,Land,Team,Manager' && C.length > 1 ){
      var c = C[1].split(',');
      $('#teamID').val(c[0]);
      $('#nt').val(c[1]);
      $('#team').val(c[2]);
      $('#manager').val(c[3]);
    }   
  }

</script>


<form id="formu" method="POST">

<div class="full">

  <p>
    <a href="teams.php"><b>Teams</b></a> | 
    <a href="turnier.php">Turnier</a>
  </p>

  <select onchange="selectTeam(this)">
  <option value="add">... Team hinzufügen ...</option>
  <?php
    foreach( db_getFromTeams() as $t ){
      ?><option value="<?= $t['id'] ?>,<?= $t['name'] ?>,<?= $t['manager'] ?>,<?= $t['nt'] ?>,<?= $t['start'] ?>" title="Manager: <?= $t['manager'] ?>]"><?= $t['id'] ?> ab <?= $t['start'] ?> - <?= $t['name'] ?> (<?= $t['nt'] ?>)</option><?
    }
  ?>
  </select>

  <table>
    <tr>
      <td>id</td><td><input id="teamID" name="teamID" type="text" value=""></td>
    </tr>
    <tr>
      <td>land</td><td><input id="nt" name="nt" type="text" value=""></td>
    </tr>
    <tr>
      <td>team</td><td><input id="team" name="team" type="text" value=""></td>
    </tr>
    <tr>
      <td>manager</td><td><input id="manager" name="manager" type="text" value=""></td>
    </tr> 
    <tr>
      <td>startsaison</td><td><input id="start" name="start" type="text" value="{{ site.data.global.saison_now }}"></td>
    </tr> 
    <tr>
      <td></td><td><button type="submit">absenden</button></td>
    </tr>
  </table>
  <textarea id="csv" name="csv" cols="50" rows="4" onpaste="setTimeout( () => ( readCsv(this) ), 200 )">
  </textarea>
</div>
</form>

