
module Is_Numeric

  def is_numeric(string)
    true if Float(string) rescue false
  end

end

Liquid::Template.register_filter(Is_Numeric) # register filter globally
